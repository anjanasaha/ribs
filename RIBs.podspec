Pod::Spec.new do |s|
  s.name             = 'RIBs'
  s.version          = '0.1.11'
  s.summary          = 'Uber\'s cross-platform mobile architecture.'
  s.description      = <<-DESC
RIBs is the cross-platform architecture behind many mobile apps at Uber. This architecture framework is designed for mobile apps with a large number of engineers and nested states.
                       DESC
  s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/ribs.git'
  s.license          = { :type => 'Apache License, Version 2.0', :file => 'LICENSE.txt' }
  s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/ribs.git', :tag => 'v' + s.version.to_s }
  s.author           = { "Anjana Saha" => "anjanasaha26@gmail.com" }
  s.ios.deployment_target = '13.0'
  s.source_files = 'ios/RIBs/Classes/**/*'
  s.dependency 'RxSwift', '~> 5.1'
  s.dependency 'RxRelay', '~> 5.1'
  s.swift_versions = '5.0'
end
